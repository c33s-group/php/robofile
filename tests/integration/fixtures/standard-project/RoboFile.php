<?php

declare(strict_types=1);

// https://github.com/squizlabs/PHP_CodeSniffer/issues/2015
// https://github.com/squizlabs/PHP_CodeSniffer/issues/2015
// phpcs:disable PSR1.Files.SideEffects
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
// phpcs:disable Generic.Files.LineLength.TooLong
// phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClass
// phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
// phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClassAfterLastUsed
// phpcs:disable Generic.CodeAnalysis.EmptyStatement.DetectedCatch
const C33S_SKIP_LOAD_DOT_ENV = true;
/*
 * =================================================================
 * Start CI auto fetch (downloading robo dependencies automatically)
 * =================================================================.
 */
const C33S_ROBO_DIR = '.robo';

$roboDir = C33S_ROBO_DIR;
$previousWorkingDir = getcwd();
(is_dir($roboDir) || mkdir($roboDir) || is_dir($roboDir)) && chdir($roboDir);
if (!is_file('composer.json')) {
    exec('composer init --no-interaction', $output, $resultCode);
    exec('composer require c33s/robofile --no-interaction', $output, $resultCode);
    exec('rm composer.yaml || rm composer.yml || return true', $output, $resultCode2);
    if ($resultCode > 0) {
        copy('https://getcomposer.org/composer.phar', 'composer');
        exec('php composer require c33s/robofile --no-interaction');
        unlink('composer');
    }
} else {
    exec('composer install --dry-run --no-interaction 2>&1', $output);
    if (false === strpos(implode((array) $output), 'Nothing to install')) {
        fwrite(STDERR, "\n##### Updating .robo dependencies #####\n\n") && exec('composer install --no-interaction');
    }
}
chdir($previousWorkingDir);
require $roboDir.'/vendor/autoload.php';
/*
 * =================================================================
 *                        End CI auto fetch
 * =================================================================.
 */

use Consolidation\AnnotatedCommand\CommandData;
use Robo\Exception\TaskException;
use Robo\Task\Remote\Rsync;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \C33s\Robo\BaseRoboFile
{
    const GLOBAL_COMPOSER_PACKAGES = [
        'hirak/prestissimo' => '^0.3',
//        'vaimo/composer-patches' => 'dev-c33s-master',
    ];

    use \C33s\Robo\C33sTasks;
    use \C33s\Robo\C33sExtraTasks;
    // use \C33s\Robo\DebugHooksTrait;
//    use \C33s\Robo\C33sExtraTasks {
//        test as testTrait;
//    }

    protected $portsToCheck = [
//        'http' => null,
//        'mysql' => null,
    ];

    /**
     * RoboFile constructor.
     */
    public function __construct()
    {
//        $this->deploymentEnvironmentConfigKey = 'deploy-env';
//        $this->deploymentEnvironmentConfigKeyShort = null;
//        $this->environmentsConfigKey = 'env';
    }

    /**
     * @hook pre-command
     *
     * @see https://ci-provider.vworld.at/
     */
    public function preCommand(CommandData $commandData)
    {
//        $this->assignEnvironmentConfig($commandData);
        if ($this->isEnvironmentCi()) {
            $this->input()->setInteractive(false);
        }

        $this->stopOnFail(true);
        $this->_prepareCiModules([
            'composer' => '1.10.10',
            'php-cs-fixer' => 'v2.16.4',
//            'phpstan' => '0.11.16', //current default location .robo/vendor
//            'phpcs' => '3.4.0', //current default location .robo/vendor
        ]);
    }

    /**
     * Initialize project.
     */
    public function init()
    {
        if (!$this->confirmIfInteractive('Have you read the README.md?')) {
            $this->abort();
        }

        if (!$this->ciCheckPorts($this->portsToCheck)) {
            if (!$this->confirmIfInteractive('Do you want to continue?')) {
                $this->abort();
            }
        }

        foreach (self::GLOBAL_COMPOSER_PACKAGES as $package => $version) {
            $this->composerGlobalRequire($package, $version);
        }

        $this->update();
//        $this->reset();
    }

    /**
     * Perform code-style checks.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function check($arguments = '')
    {
        $this->checkPhpCsniff($arguments);
        $this->checkPhpCsFix($arguments);
        $this->checkPhpstan($arguments);
//        $this->checkTwigCs($arguments);
//        $this->checkSymfony($arguments);
//        $this->checkDoctrine($arguments);
    }

    public function checkPhpCsniff($arguments = '')
    {
        $this->_execPhpQuiet('php .robo/vendor/squizlabs/php_codesniffer/bin/phpcs '.$arguments);
//        $this->_execPhpQuiet('php .robo/bin/phpcs.phar');
    }

    public function checkPhpCsFix($arguments = '')
    {
        $this->_execPhpQuiet("php .robo/bin/php-cs-fixer.phar fix --verbose --dry-run $arguments");
    }

    public function checkPhpstan($arguments = '')
    {
        $this->_execPhpQuiet('php .robo/vendor/phpstan/phpstan/phpstan analyse -c phpstan.neon');
//        $this->_execPhpQuiet('php .robo/bin/phpstan.phar analyse -c phpstan.neon');
    }

    public function checkTwigCs($arguments = '')
    {
        $this->_execPhp('php .robo/vendor/friendsoftwig/twigcs/bin/twigcs');
    }

    public function checkSymfony($arguments = '')
    {
        $this->_execPhp('php bin/console lint:yaml config --parse-tags');
        $this->_execPhp('php bin/console lint:twig templates --env=prod');
        $this->_execPhp('php bin/console lint:container');
//        $this->_execPhp('php bin/console lint:xliff translations');
//        $this->_execPhp('php bin/console security:check');
    }

    public function checkDoctrine($arguments = '')
    {
        $this->_execPhp('php bin/console doctrine:schema:validate --skip-sync -vvv --no-interaction');
        $this->_execSymfony('php bin/console doctrine:schema:validate --skip-sync');
    }

    /**
     * Perform code-style checks and cleanup source code automatically.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function fix($arguments = '')
    {
        if ($this->confirmIfInteractive('Do you really want to run php-cs-fixer on your source code?')) {
            $this->_execPhp("php .robo/bin/php-cs-fixer.phar fix --verbose $arguments");
        } else {
            $this->abort();
        }
    }

    /**
     * Update the Project.
     */
    public function update()
    {
        if ($this->isEnvironmentCi() || $this->isEnvironmentProduction()) {
            $this->_execPhp('php ./.robo/bin/composer.phar install --no-progress --no-suggest --prefer-dist --optimize-autoloader');
        } else {
            $this->_execPhp('php ./.robo/bin/composer.phar install');
        }
//        $this->assetsInstall();
    }

    /**
     * Re-crates a fresh database (be careful all tables will be deleted).
     *
     * @param array $opts
     */
    public function reset($opts = ['limit' => 'none', 'bootstrap' => false, 'no-cache-clear' => false, 'vvv' => false, 'environments' => []])
    {
        $environments = $opts['environments'];

        if ($this->isEnvironmentProduction()) {
            if (!$this->confirmIfInteractive('THIS WILL DESTROY ALL EXISTING DATA IN YOUR PROJECT\'S DATABASE! CONTINUE?')) {
                $this->abort();
            }
            // only init prod envs on production to not overload fixtures https://gitlab.com/consistency/customer/knapplab.com/issues/189
            if (empty($environments)) {
                $environments = ['prod'];
            }
        }

        if (empty($environments)) {
            $environments = ['dev', 'test'];
        }

        foreach ($environments as $environment) {
            $this->_execSymfony("php bin/console doctrine:schema:drop --full-database --force --env={$environment}");
            $this->_execSymfony("php bin/console doctrine:schema:create --no-interaction --env={$environment}");
//            if ('test' === $environment) {
//                $this->_execSymfony('php bin/console messenger:setup-transports --env=test');
//            }
//            $this->_execSymfony("php bin/console doctrine:fixtures:load --no-interaction --env=${environment}");
            if (false === $opts['no-cache-clear']) {
                $this->_execSymfony("php ./bin/console cache:clear --no-warmup --env={$environment}");
                $this->_execSymfony("php ./bin/console cache:warmup --env={$environment}");
            }
        }
    }

    public function db($opts = ['environment' => 'dev'])
    {
        $this->reset(['limit' => 'none', 'bootstrap' => false, 'no-cache-clear' => true, 'vvv' => false, 'environments' => [$opts['environment']]]);
    }

    /**
     * This will be executed on the target machine after syncing all the files during robo:deploy.
     */
    public function deployAfter()
    {
        $this->_exec('hostname --fqdn');
        $this->update(); // does a composer install
        $this->_execPhp('php ./bin/console cache:clear --no-warmup');
        $this->_execPhp('php ./bin/console cache:warmup');
    }

    /**
     * Unfortunately cloning the rsync task does not work as advertised:.
     *
     * Ssh & Rsync Task make wrong quoting
     *
     * @see https://github.com/consolidation/Robo/issues/583
     * @see https://github.com/consolidation/Robo/issues/645
     *
     * @throws TaskException
     * @throws Exception
     */
    protected function buildRsyncTaskForDeploy(): Rsync
    {
        $data = $this->getRemoteDataArray();

        /** @var string $user */
        $user = $data['user'];

        /** @var string $hostname */
        $hostname = $data['hostname'];

        /** @var int $port */
        $port = $data['port'];

        /** @var string $path */
        $path = $data['path'];

        /** @var string $key */
        $key = $data['key'];

        $sshBinary = $this->getEnv('RSYNC_SSH', 'ssh');

        $rsyncTask = $this->taskRsync()
            ->toUser($user)
            ->rawArg("-e \"{$sshBinary} -p {$port}{$key}\"")
            ->option('delete-after')
            ->fromPath('./')
            ->toPath("{$hostname}:{$path}")
            ->humanReadable()
            ->verbose()
            ->progress()
            ->checksum()
            ->delete()
            ->recursive()
            ->rawArg('--chmod ug+rwX')
            ->rawArg("--chown $user:www-data")
            ->rawArg('--perms')
            ->exclude('public/.htpasswd')
//            ->exclude('.env')
            ->exclude('.env.dist')
            ->exclude('public/.htaccess')
            ->excludeVcs()
//            ->exclude('var/')
//            ->filter(':- .gitignore')
//            ->filter(':- .git/info/exclude')
//            https://github.com/consolidation/Robo/issues/728
//            ->remoteShell("${sshBinary} -p ${port}${key}")
//            ->rawArg("-rsh \"${sshBinary} -p ${port}${key}\"")
//            ->remoteShell('ssh-cw -i id_rsa') // sadly leads to "'ssh-cw -i id_rsa'" -> symfony process escape helper problem
//            ->option('perms')
//            ->fromPath(__DIR__.'/')
//            ->option('no-owner')
//            ->option('no-group')
//            ->dryRun()
//            ->option('usermap=xxxxxx:xxxxx')
//            ->rawArg('-og --chown=apache:apache')
//            ->owner()
//            ->group()
//            ->times()
//            ->group()
//            ->owner()
        ;

        if (file_exists('.git/info/exclude')) {
            $rsyncTask->excludeFrom('.git/info/exclude');
        }
        if (file_exists('.gitignore')) {
            $rsyncTask->excludeFrom('.gitignore');
        }
        if (file_exists('.rsync-exclude')) {
            $rsyncTask->excludeFrom('.rsync-exclude');
        }

        return $rsyncTask;
    }
}
