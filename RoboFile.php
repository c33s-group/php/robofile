<?php

const C33S_SKIP_LOAD_DOT_ENV = true;
/*
 * =================================================================
 * Start CI auto fetch (downloading robo dependencies automatically)
 * =================================================================.
 */
const C33S_ROBO_DIR = '.robo';

$roboDir = C33S_ROBO_DIR;
$previousWorkingDir = getcwd();
(is_dir($roboDir) || mkdir($roboDir) || is_dir($roboDir)) && chdir($roboDir);
if (!is_file('composer.json')) {
    exec('composer init --no-interaction', $output, $resultCode);
    exec('composer require c33s/robofile --no-interaction', $output, $resultCode);
    exec('rm composer.yaml || rm composer.yml || return true', $output, $resultCode2);
    if ($resultCode > 0) {
        copy('https://getcomposer.org/composer.phar', 'composer');
        exec('php composer require c33s/robofile --no-interaction');
        unlink('composer');
    }
} else {
    exec('composer install --dry-run --no-interaction 2>&1', $output);
    if (false === strpos(implode((array) $output), 'Nothing to install')) {
        fwrite(STDERR, "\n##### Updating .robo dependencies #####\n\n") && exec('composer install --no-interaction');
    }
}
chdir($previousWorkingDir);
require $roboDir.'/vendor/autoload.php';
/*
 * =================================================================
 *                        End CI auto fetch
 * =================================================================.
 */

/*
 * =================================================================
 * Autoloader for Development, remove this part from the file if you
 * want to use it in project environment.
 * =================================================================.
 */
$output = '';
exec('composer install --dry-run --no-interaction 2>&1', $output);
if (false === strpos(implode((array) $output), 'Nothing to install')) {
    fwrite(STDERR, "\n##### Updating vendor #####\n\n") && exec('composer install --no-interaction');
}
require 'vendor/autoload.php';
/*
 * =================================================================
 *                        End Autoloader for Development
 * =================================================================.
 */

use C33s\Robo\Task\ExecWinSlashes\ExecWinSlashes;
use Consolidation\AnnotatedCommand\CommandData;
use Robo\Collection\CollectionBuilder;
use Robo\Task\Base\Exec;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \C33s\Robo\BaseRoboFile
{
    use \C33s\Robo\C33sTasks;
    use \C33s\Robo\C33sExtraTasks;
    // use \C33s\Robo\DebugHooksTrait;

    protected $portsToCheck = [
        // 'http' => null,
        // 'https' => null,
        // 'mysql' => null,
        // 'postgres' => null,
        // 'elasticsearch' => null,
        // 'mongodb' => null,
    ];

    /**
     * @var string
     */
    protected $baseDir = 'tests/integration';

    /**
     * @var string
     */
    protected $projectDir = 'tests/integration/project';

    /**
     * @var string
     */
    protected $packageDir = 'tests/integration/package';

    /**
     * @var string
     */
    protected $fixtureDir = 'tests/integration/fixtures';

    /**
     * @hook pre-command
     */
    public function preCommand(CommandData $commandData)
    {
        if ($this->isEnvironmentCi() || $this->isEnvironmentProduction()) {
            $this->input()->setInteractive(false);
        }
        $this->assignEnvironmentConfig($commandData);
        $this->stopOnFail(true);
        $this->_prepareCiModules([
            'composer' => '2.1.6',
            'php-cs-fixer' => 'v2.16.4',
        ]);
    }

    /**
     * Initialize project.
     */
    public function init()
    {
        if (!$this->confirmIfInteractive('Have you read the README.md?')) {
            $this->abort();
        }

        if (!$this->ciCheckPorts($this->portsToCheck)) {
            if (!$this->confirmIfInteractive('Do you want to continue?')) {
                $this->abort();
            }
        }
    }

    /**
     * Perform code-style checks.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function check($arguments = '')
    {
        $this->checkFixer($arguments);
        $this->checkStan($arguments);
        $this->checkSniff($arguments);
    }

    public function checkSniff($arguments = '')
    {
        $this->_execPhpQuiet("php {$this->dir()}/vendor/squizlabs/php_codesniffer/bin/phpcs $arguments");
    }

    public function checkFixer($arguments = '')
    {
        $this->_execPhpQuiet("php {$this->dir()}/bin/php-cs-fixer.phar fix --verbose --dry-run $arguments");
    }

    public function checkStan($arguments = '')
    {
        $this->_execPhpQuiet("php {$this->dir()}/vendor/phpstan/phpstan/phpstan analyse -c phpstan.neon $arguments");
    }

    /**
     * Perform code-style checks and cleanup source code automatically.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function fix($arguments = '')
    {
        if ($this->confirmIfInteractive('Do you really want to run php-cs-fixer on your source code?')) {
            $this->_execPhp("php {$this->dir()}/bin/php-cs-fixer.phar fix --verbose $arguments");
        } else {
            $this->abort();
        }
    }

    /**
     * Run tests.
     */
    public function test()
    {
        $this->testIntegration();
    }

    /**
     * Run the integration tests.
     */
    public function testIntegration()
    {
        $this->testIntegrationClean();

        $this->taskCopyDir([$this->fixtureDir.'/standard-project' => $this->projectDir])
            ->overwrite(true)
            ->run()
        ;
        $this->getProjectBaseRoboTask('init')->run();
        $this->failOnNotExist($this->projectDir.'/.robo');
        $this->failOnNotExist($this->projectDir.'/.robo/composer.lock');
        $this->failOnNotExist($this->projectDir.'/.robo/bin/composer.phar');
        $this->failOnNotExist($this->projectDir.'/.robo/bin/php-cs-fixer.phar');
        $this->failOnNotExist($this->projectDir.'/.robo/cache/ci-provider.json');
        $this->failOnNotExist($this->projectDir.'/.robo/vendor/autoload.php');
        $this->getProjectBaseRoboTask('check')->run();
    }

    /**
     * Clean the integration tests directories and initialize the package directory.
     */
    public function testIntegrationClean()
    {
        $integrationTestDirs = [$this->packageDir, $this->projectDir];
        $this->deleteDirWindowsSafe($integrationTestDirs);
        $this->_mkdir($integrationTestDirs);
        $this->taskCopyDir(['./' => $this->packageDir])
            ->exclude([
                '.git',
                'vendor',
                'tests',
                '.robo',
                'app',
                'doc',
            ])
            ->overwrite(true)
            ->run()
        ;
    }

    /**
     * Returns a basic Robo task for the integration test project.
     *
     * @param string $roboCommand
     *
     * @return ExecWinSlashes|CollectionBuilder|Exec
     */
    protected function getProjectBaseRoboTask($roboCommand)
    {
        return $this->taskExec('robo')
            ->arg($roboCommand)
            ->dir($this->projectDir)
            ->options(['no-interaction' => null])
        ;
    }
}
