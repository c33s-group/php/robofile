<?php

namespace C33s\Robo;

trait DebugHooksTrait
{
    /**
     * @hook pre-init
     */
    public function debugPreInit()
    {
        $this->_debugHook('pre-init', func_get_args());
    }

    /**
     * @hook init
     */
    public function debugDoInit()
    {
        $this->_debugHook('init', func_get_args());
    }

    /**
     * @hook post-init
     */
    public function debugPostInit()
    {
        $this->_debugHook('post-init', func_get_args());
    }

    /**
     * @hook pre-alter
     */
    public function debugPreAlter()
    {
        $this->_debugHook('pre-alter', func_get_args());
    }

    /**
     * @hook alter
     */
    public function debugDoAlter()
    {
        $this->_debugHook('alter', func_get_args());
    }

    /**
     * @hook post-alter
     */
    public function debugPostAlter()
    {
        $this->_debugHook('post-alter', func_get_args());
    }

    /**
     * @hook pre-process
     */
    public function debugPreProcess()
    {
        $this->_debugHook('pre-process', func_get_args());
    }

    /**
     * @hook process
     */
    public function debugDoProcess()
    {
        $this->_debugHook('process', func_get_args());
    }

    /**
     * @hook post-process
     */
    public function debugPostProcess()
    {
        $this->_debugHook('post-process', func_get_args());
    }

    /**
     * @hook pre-option
     */
    public function debugPreOption()
    {
        $this->_debugHook('pre-option', func_get_args());
    }

    /**
     * @hook option
     */
    public function debugDoOption()
    {
        $this->_debugHook('option', func_get_args());
    }

    /**
     * @hook post-option
     */
    public function debugPostOption()
    {
        $this->_debugHook('post-option', func_get_args());
    }

    /**
     * @hook pre-command-event
     */
    public function debugPreCommandEvent()
    {
        $this->_debugHook('pre-command-event', func_get_args());
    }

    /**
     * @hook command-event
     */
    public function debugDoCommandEvent()
    {
        $this->_debugHook('command-event', func_get_args());
    }

    /**
     * @hook post-command-event
     */
    public function debugPostCommandEvent()
    {
        $this->_debugHook('post-command-event', func_get_args());
    }

    /**
     * @hook pre-interact
     */
    public function debugPreInteract()
    {
        $this->_debugHook('pre-interact', func_get_args());
    }

    /**
     * @hook interact
     */
    public function debugDoInteract()
    {
        $this->_debugHook('interact', func_get_args());
    }

    /**
     * @hook post-interact
     */
    public function debugPostInteract()
    {
        $this->_debugHook('post-interact', func_get_args());
    }

    /**
     * @hook pre-command
     */
    public function debugPreCommand()
    {
        $this->_debugHook('pre-command', func_get_args());
    }

    /**
     * Print debug information for the given hook.
     *
     * @param string $name
     */
    protected function _debugHook($name, array $args)
    {
        $fgColor = 'blue';
        $bgColor = 'black';
        $format = "  <fg=$fgColor;bg=$bgColor;options=bold>%s</fg=$fgColor;bg=$bgColor;options=bold>";

        $this->writeln(sprintf($format, "TRIGGERED HOOK: $name"));
        $i = 0;
        foreach ($args as $arg) {
            $class = get_class($arg);
            $this->writeln(sprintf($format, "  argument $i: $class"));
            ++$i;
        }
    }
}
