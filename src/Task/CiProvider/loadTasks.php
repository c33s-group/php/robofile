<?php

namespace C33s\Robo\Task\CiProvider;

trait loadTasks
{
    /**
     * Download all given ci provider modules.
     *
     * @param array $modules List of modules [$name => $version]
     *
     * @return DownloadModules
     */
    protected function taskCiProviderDownloadModules($modules) //TODO: php7 - array
    {
        return $this->task(DownloadModules::class, $modules);
    }

    /**
     * Fetch the list of available CI provider modules.
     *
     * @return FetchModulesFile
     */
    protected function taskCiProviderRefreshModulesFile()
    {
        return $this->task(FetchModulesFile::class);
    }

    /**
     * Download the given CI modules if not present. Expects an array like this:
     * [
     *     'name1' => 'version1',
     *     'name2' => 'version2',
     * ].
     */
    protected function _prepareCiModules(array $modules)
    {
        $this
            ->taskCiProviderDownloadModules($modules)
            ->run()
        ;
    }
}
