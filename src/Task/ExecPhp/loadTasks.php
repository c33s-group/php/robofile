<?php

namespace C33s\Robo\Task\ExecPhp;

use Robo\Contract\CommandInterface;
use Robo\Exception\TaskExitException;
use Robo\Result;

trait loadTasks
{
    /**
     * Execute php script or PHAR container using auto-detected PHP executable.
     * Allows enabling xdebug.
     *
     * @param string|CommandInterface $command
     *
     * @return ExecPhp
     */
    protected function taskExecPhp($command)
    {
        return $this->task(ExecPhp::class, $command);
    }

    /**
     * @param string|CommandInterface $command
     * @param bool                    $enableXDebug
     *
     * @return Result
     */
    protected function _execPhp($command, $enableXDebug = false)
    {
        return $this->taskExecPhp($command)
            ->enableXDebug($enableXDebug)
            ->run()
        ;
    }

    /**
     * On `execs` robo shows the text of the exception directly from the command executed and after the command was run
     * it also shows the output text which also contains the exception text. to prevent this use this exec.
     *
     * @param $command
     */
    protected function _execPhpQuiet($command)
    {
        try {
            $this->_execPhp($command);
        } catch (TaskExitException $exception) {
            $this->throwReducedErrorException();
        }
    }
}
