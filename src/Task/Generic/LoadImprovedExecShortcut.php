<?php

namespace C33s\Robo\Task\Generic;

trait LoadImprovedExecShortcut
{
    /**
     * Executes shell command and allows to add variables to the environment.
     *
     * @param $command string|\Robo\Contract\CommandInterface $command
     * @param array     $mergeEnvironment
     * @param null      $environment
     * @param bool|null $interactive
     *
     * @return \Robo\Result
     */
    protected function _exec($command, $mergeEnvironment = [], $environment = null, $interactive = false)
    {
        $task = $this->taskExec($command)->interactive($interactive);
        if (!empty($mergeEnvironment)) {
            if (!$environment) {
                $environment = $_ENV;
            }
            $task->envVars(array_merge($mergeEnvironment, $environment));
        }

        return $task->run();
    }
}
