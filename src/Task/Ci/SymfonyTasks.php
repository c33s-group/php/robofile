<?php

namespace C33s\Robo\Task\Ci;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

trait SymfonyTasks
{
    protected $symfonyParameters = [];

    /**
     * Load Symfony parameters from parameters.yml.
     *
     * @param string $path
     *
     * @return array
     */
    protected function loadSymfonyParameters($path = 'app/config/parameters.yml')
    {
        $file = getcwd().'/'.$path;
        $parameters = [];
        if (file_exists($file.'.dist')) {
            $parameters = $this->doLoadParametersFile($file.'.dist');
        }
        if (file_exists($file)) {
            $parameters = array_merge($parameters, $this->doLoadParametersFile($file));
        } else {
            $this->say("<error>Could not find parameters file $path</error>");
        }

        $this->symfonyParameters = $parameters;

        return $this->symfonyParameters;
    }

    /**
     * @param string $file
     *
     * @return array
     */
    protected function doLoadParametersFile($file)
    {
        try {
            $data = Yaml::parse(file_get_contents($file));
            if (isset($data['parameters'])) {
                return $data['parameters'];
            }
        } catch (ParseException $e) {
            $this->say(sprintf(
                '<error>There was an error parsing the parameters file %s: %s</error>',
                $file,
                $e->getMessage()
            ));
        }

        return [];
    }

    /**
     * Resolve a symfony parameter in the given value if present.
     * This is a rather dirty implementation which will work for most cases.
     *
     * @param $value
     *
     * @return mixed
     */
    protected function _resolveSymfonyParameter($value)
    {
        if (!preg_match('/^%.*%$/', $value)) {
            return $value;
        }

        $name = trim($value, '%');
        $parameters = $this->loadSymfonyParameters();
        if (isset($parameters[$name])) {
            if ($this->output()->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                $this->writeln("Resolved parameter <info>$value</info> to value <info>{$parameters[$name]}</info>");
            }

            return $parameters[$name];
        }

        return $value;
    }
}
