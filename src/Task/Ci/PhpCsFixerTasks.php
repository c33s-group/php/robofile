<?php

namespace C33s\Robo\Task\Ci;

use Robo\Result;

trait PhpCsFixerTasks
{
    /**
     * Download all given ci provider modules.
     *
     * @param      $arguments
     * @param bool $dryRun
     *
     * @return Result
     */
    protected function _runPhpCsFixer($arguments = '', $dryRun = true)
    {
        $arguments = (string) $arguments;
        $dryRun = $dryRun ? '--dry-run' : '';

        return $this
            ->taskExecPhp("php {$this->dir()}/bin/php-cs-fixer.phar fix -v $dryRun $arguments")
            ->run()
        ;
    }
}
