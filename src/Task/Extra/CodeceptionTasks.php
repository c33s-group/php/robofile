<?php

namespace C33s\Robo\Task\Extra;

trait CodeceptionTasks
{
    /**
     * Run tests.
     */
    public function test($group = '', $suite = '', $opts = ['coverage' => false])
    {
        \C33s\Robo\Task\Symfony\ExecSymfonyTask::setDefaultEnvironment('test');

        if ($group) {
            $group = "-g $group";
        }
        if ($this->isEnvironmentCi() || $opts['coverage']) {
            $this->_execPhpQuiet("php ./vendor/codeception/codeception/codecept run $suite $group --coverage-xml --coverage-html --coverage-text", true);
            $this->outputCoverage();
        } else {
            $this->_execPhpQuiet("php ./vendor/codeception/codeception/codecept run $suite $group");
        }
    }

    /**
     * Run Unit tests.
     */
    public function testUnit($group = '')
    {
        $this->test($group, 'unit');
    }

    /**
     * Run Functional tests.
     */
    public function testFunctional($group = '')
    {
        $this->test($group, 'functional');
    }

    /**
     * Write plain coverage line used for gitlab CI detecting the coverage score.
     */
    private function outputCoverage() //TODO: php7 : void
    {
        $dirPrefix = '';
        if (getenv('PWD')) {
            $dirPrefix = getenv('PWD').'/';
        }
        $this->writeln(file($dirPrefix.'tests/_output/coverage.txt')[8]);
    }
}
