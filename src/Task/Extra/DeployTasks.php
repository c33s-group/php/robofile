<?php

namespace C33s\Robo\Task\Extra;

use Exception;

trait DeployTasks
{
    protected $target = 'staging';

    /**
     * @return string
     *
     * @throws Exception
     */
    protected function getRemoteHostname() //TODO: php7 -: string
    {
        $this->checkEnvWithTarget(['HOST']);

        return $this->getEnvWithTarget('HOST');
    }

    /**
     * @return string
     *
     * @throws Exception
     */
    protected function getRemoteUser() //TODO: php7 -: string
    {
        $this->checkEnvWithTarget(['USERNAME']);

        return $this->getEnvWithTarget('USERNAME');
    }

    /**
     * @return string
     *
     * @throws Exception
     */
    protected function getRemotePort() //TODO: php7 -: string
    {
        return $this->getEnvWithTarget('PORT', 22);
    }

    /**
     * @return string|null
     *
     * @throws Exception
     */
    protected function getRemoteKey() //TODO: php7 -: ?string
    {
        return $this->getEnvWithTarget('KEY_PRIVATE_PATH');
    }

    protected function checkEnvWithTarget(array $names)
    {
        $namesNew = array_map(function ($element) { return strtoupper($this->target).'_'.$element; }, $names);
        $this->checkEnv($namesNew);
    }

    /**
     * @param string $name
     * @param null   $default
     *
     * @return mixed|null
     *
     * @throws Exception
     */
    protected function getEnvWithTarget($name, $default = null) //TODO: php7 -: string
    {
        if (!$this->target) {
            throw new Exception('no target specified');
        }
        $nameNew = strtoupper($this->target).'_'.$name;

        return $this->getEnv($nameNew, $default);
    }

    protected function getRemotePath() //TODO: php7 -: string
    {
        $this->checkEnvWithTarget(['PATH']);
        $path = $this->getEnvWithTarget('PATH');
        $path = trim($path);
        $path = rtrim($path, '/');
        if ('/' === $path) {
            $this->abort('Cannot deploy to / for security reasons');
        }
        $path = rtrim($path, '/');

        return $path;
    }

    /**
     * Execute command remotely on deployment host and path.
     *
     * Ssh & Rsync Task make wrong quoting
     * see https://github.com/consolidation/Robo/issues/645
     *
     * @param string $command
     *
     * @throws Exception
     */
    protected function remoteExec($command, $sshBin = 'ssh') //TODO: php7 string
    {
        extract($this->getRemoteDataArray());
        /*
         * @var string $port
         * @var string $key
         * @var number $hostname
         * @var string $path
         * @var string $user
         */

        $this->_exec("{$sshBin} -p {$port}{$key} {$user}@{$hostname} \"cd {$path} && {$command}\"");
//        https://github.com/consolidation/Robo/issues/728
//        $task = $this->taskSshExec($hostname)
//            ->remoteDir($path)
//            ->port($port)
//            ->user($user)
//            ->exec($command)
//            ->run()
//            ->identityFile('id_rsa')
//            ->verbose()
//        ;
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    protected function getRemoteDataArray()
    {
//        $hostname = $this->getRemoteHostname();
//        $path = $this->getRemotePath();
//        $user = $this->getRemoteUser();
//        $port = $this->getRemotePort();
//        $key = $this->getRemoteKey() ? ' -i '.$this->getRemoteKey() : '';

        $data = [
            'hostname' => $this->getRemoteHostname(),
            'path' => $this->getRemotePath(),
            'user' => $this->getRemoteUser(),
            'port' => $this->getRemotePort(),
            'key' => $this->getRemoteKey() ? ' -i '.$this->getRemoteKey() : '',
        ];

        return $data;
    }

    /**
     * Deploy to remote server. Set DEPLOY_PATH and DEPLOY_HOSTNAME (defaults to "production") before running this.
     *
     * @throws TaskException
     * @throws Exception
     */
    public function deploy($target = 'staging')
    {
        $this->versionUpdate();
        $this->target = $target;
        extract($this->getRemoteDataArray());
        /*
         * @var string $port
         * @var string $key
         * @var number $hostname
         * @var string $path
         * @var string $user
         */
//        $hostname = $this->getRemoteHostname();
//        $path = $this->getRemotePath();
//        $user = $this->getRemoteUser();

        $this->writeln('');
        $this->yell("Deploying to $user@$hostname:$path");
        $this->writeln('');

        if ($this->input->isInteractive()) {
            $this->buildRsyncTaskForDeploy()->dryRun()->run();
            $this->writeln('');
            if (!$this->confirmIfInteractive("Do you want to live execute the above dry-run to $hostname:$path?")) {
                $this->abort();
            }
        }
        $this->buildRsyncTaskForDeploy()->run();
        $this->remoteExec('robo deploy:after');
    }
}
