<?php

namespace C33s\Robo\Task\Extra;

use Exception;

trait ErrorHandling
{
    /**
     * @var int
     */
    protected $errorCount = 0;

    /**
     * Prevent duplicate output with robo.
     * Used by for example `_execPhpQuiet`.
     */
    protected function throwReducedErrorException($message = null)
    {
        if (null === $message) {
            $message = 'Reduced error Exception, the real exception output should be shown above';
        }
        ++$this->errorCount;
        throw new Exception($message);
    }
}
