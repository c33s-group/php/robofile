<?php

namespace C33s\Robo;

use C33s\Robo\Task\Ci\ComposerTasks;
use C33s\Robo\Task\Ci\GenericCiTasks;
use C33s\Robo\Task\Ci\PhpCsFixerTasks;
use C33s\Robo\Task\Ci\SymfonyTasks;
use C33s\Robo\Task\CiProvider\loadTasks as LoadCiProviderTasks;
use C33s\Robo\Task\ExecPhp\loadTasks as LoadPhpTasks;
use C33s\Robo\Task\ExecWinSlashes\loadTasks as ExecWinSlashesTasks;
use C33s\Robo\Task\FileDownload\loadTasks as LoadFileDownloadTasks;
use C33s\Robo\Task\Generic\DeploymentEnvironments;
use C33s\Robo\Task\Generic\Environments;
use C33s\Robo\Task\Generic\GenericTasks;
use C33s\Robo\Task\Generic\LoadImprovedExecShortcut;
use C33s\Robo\Task\Symfony\loadTasks as LoadSymfonyTasks;

/*
 * TODO: refactor dotenv loading
 */
DotenvWrapper::loadIfExists('./.env.dist', './.env');

trait C33sTasks
{
    use ComposerTasks;
    use ExecWinSlashesTasks;
    use GenericCiTasks;
    use GenericTasks;
    use LoadCiProviderTasks;
    use LoadFileDownloadTasks;
    use LoadPhpTasks;
    use DeploymentEnvironments;
    use Environments;
    use PhpCsFixerTasks;
    use SymfonyTasks;
    use LoadImprovedExecShortcut;
    use LoadSymfonyTasks;
}
